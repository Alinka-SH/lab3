attrs==22.1.0
colorama==0.4.6
exceptiongroup==1.0.4
future==0.18.2
iniconfig==1.1.1
packaging==21.3
pluggy==1.0.0
pyparsing==3.0.9
pytest==7.2.0
tk==0.1.0
tomli==2.0.1
